#
# docker build -t eicdev/ejana-ubuntu18-prereq:latest .  
# docker build -t eicdev/ejana-ubuntu18-prereq:latest --build-arg BUILD_THREADS=24 .
# docker push eicdev/ejana-ubuntu18-prereq:latest
# 
# docker run --rm -it --init eicdev/ejana-ubuntu18-prereq:latest
# docker run --rm -it --init -p8888:8888 eicdev/ejana-ubuntu18-prereq:latest

FROM ubuntu:18.04

# Number of build threads
ARG BUILD_THREADS=8

ENV EIC_ROOT /eic
ENV CONTAINER_ROOT /container
ENV APP_ROOT /container/app

SHELL ["/bin/bash", "-c"]

RUN apt-get update &&\
    apt-get install -y python3-pip python3-dev sudo git cmake &&\
    rm -rf /var/lib/apt/lists/* &&\
    pip3 install --upgrade pip 
    

RUN useradd -m -G sudo eicuser
RUN echo "eicuser ALL=(ALL) NOPASSWD:ALL">>/etc/sudoers

RUN install -d -o eicuser -g eicuser ${CONTAINER_ROOT} && \
    install -d -o eicuser -g eicuser ${APP_ROOT} && \
    install -d -o eicuser -g eicuser /eic

USER eicuser
WORKDIR /home/eicuser
ENV PATH "$PATH:/home/eicuser/.local/bin"

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8


RUN python3 -m pip install --user --upgrade --force-reinstall  click &&\
    python3 -m pip install --user --upgrade --force-reinstall  appdirs &&\
    python3 -m pip install --user --upgrade --force-reinstall  ejpm &&\
    ejpm --top-dir=/container/app

# install all packets but not ejana
RUN  sudo apt-get update &&\
     sudo apt-get -y install $(ejpm req --all ubuntu) &&\
     sudo rm -rf /var/lib/apt/lists/*

# ejana stack
RUN ejpm install clhep && ejpm clean clhep &&\    
    ejpm install hepmc && ejpm clean hepmc &&\
    ejpm install -j${BUILD_THREADS} root && ejpm clean root &&\
    ejpm install ejana --deps-only 

# g4e stack
RUN ejpm install -j${BUILD_THREADS} geant && ejpm clean geant &&\
    ejpm install g4e --deps-only

# jupyterab
RUN python3 -m pip install jupyterlab

ADD test_jsroot.ipynb /home/eicuser/

RUN echo "done"

CMD source /container/app/root/root-v6-20-00/bin/thisroot.sh &&\
    jupyter lab --ip=0.0.0.0 --no-browser \
    --NotebookApp.custom_display_url=http://127.0.0.1:8888 \
    --NotebookApp.token=''