# Import external ejpm DB to your local install
# (from ejpm which was used to build different packages)
ejpm mergedb /container/app/ejpm_data/db.json > /dev/null

# ejpm updates the next file:
source /container/app/ejpm_data/env.sh

export VISIBLE=now