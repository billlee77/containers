#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"

using namespace Pythia8;

int main( int argc, char* argv[] ) {

  // Correct number of inputs?
  if (argc != 3) {
    cout << endl
         << "  Please provide a cmnd and HepMC file!"
         << endl;
    return 1;
  }

  // Generator.
  Pythia pythia;

  // Read in commands from external file.
  pythia.readFile(argv[1]);

  // Initialize.
  pythia.init();

  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;

  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);

  // Switch off warnings for parton-level events.
  ToHepMC.set_print_inconsistency(false);
  ToHepMC.set_free_parton_exception(false);

  // Begin event loop. Generate event. Skip if error.
  int nEvent = pythia.settings.mode("Main:numberOfEvents");
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;
    

    // Construct new empty HepMC event and fill it.
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( pythia, hepmcevt );

    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;

  } // End loop over events to generate.

  // Print statistics.
  pythia.stat();

  // Done.
  return 0;
}
