# python3 build_images.py <flags> <name>
# 
# to build and push the "latest" version:
#    python3 build_images.py --no-cache --push eic
# to build and push the tagged version
#    python3 build_images.py --no-cache --push --tag=<name> eic
#
# Flags:
#  --no-cache - clean build
#  --push     - push after build
#  --tag      - tag name of an image (this is like "latest", not full docker name)
# Names:
#    eic - collection of eic images
#    devops - devops images,
#    or image name without namespace: escalate


import inspect
import json
import os
from os import path
import shlex
import subprocess
import argparse
import multiprocessing
from datetime import datetime
from typing import Union, List

# Set pathes and global variables
this_path = path.dirname(path.abspath(inspect.stack()[0][1]))
devops_root_path = path.join(this_path,'../../devops')
eic_root_path = path.join(this_path, '../../eic')
cpu_count = multiprocessing.cpu_count()
if cpu_count > 1:
    cpu_count -= 1

class ImageInfo:
    def __init__(self, name: str, depends_on:str = '', flags:str=''):
        self.name = name
        self.depends_on = depends_on
        self.tag = ''       # unknown yet. Set by build process
        self.path = ''      # unknown yet. Set by build process
        self.flags = flags  # Additional flags needed to build

    def __repr__(self):
        return self.name


# List of images (and its dependencies)
images = {
    'devops': [
        ImageInfo('ejana-centos7-prereq', flags='--squash'),  # doesn't depends on images from this collection
        ImageInfo('g4e-centos7-prereq', depends_on='ejana-centos7-prereq'),
        ImageInfo('g4e-centos7', depends_on='g4e-centos7-prereq'),
        ImageInfo('ejana-ubuntu18-prereq')],    
    'eic': [
        ImageInfo('eic-science', flags='--build-arg BUILD_THREADS={}'.format(cpu_count)),
        ImageInfo('eic-mceg', depends_on='eic-science'),
        ImageInfo('escalate', depends_on='eic-mceg'),
    ]
}

images['all'] = images['eic'] + images['devops']

images_by_name = {image.name: image for image in images['all']}


def _run(command: Union[str, list]) -> (int, datetime, datetime, list):
    """Wrapper around subprocess.Popen that returns:


    :return retval, start_time, end_time, lines

    """
    if isinstance(command, str):
        command = shlex.split(command)

    # Pretty header for the command
    pretty_header = "RUN: " + " ".join(command)
    print('=' * len(pretty_header))
    print(pretty_header)
    print('=' * len(pretty_header))

    # Record the start time
    start_time = datetime.now()
    lines = []

    # stderr is redirected to STDOUT because otherwise it needs special handling
    # we don't need it and we don't care as C++ warnings generate too much stderr
    # which makes it pretty much like stdout
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while True:
        output = process.stdout.readline().decode('latin-1')

        if process.poll() is not None and output == '':
            break
        if output:
            print(output, end="")
            lines.append(output)

    # Get return value and finishing time
    retval = process.poll()
    end_time = datetime.now()

    print("------------------------------------------")
    print(f"RUN DONE. RETVAL: {retval} \n\n")

    return retval, start_time, end_time, lines


class DockerAutomation(object):

    def __init__(self, images: List[ImageInfo]):  # like "ejana-centos7-prereq"
        self.operation_logs: List[dict] = []
        self.images = images
        self._images_by_name = {image.name: image for image in self.images}
        self.built_images_by_name = {}
        self.check_deps = True
        self.no_cache = False
        self.push_after_build = True

    def _append_log(self, op, name, ret_code, start_time, end_time, output):
        """Saves data to specially formatted record"""
        duration = end_time - start_time
        self.operation_logs.append({'op':op,
                                    'name': name,
                                    'ret_code': ret_code,
                                    'start_time': start_time,
                                    'start_time_str': start_time.strftime("%Y-%m-%d %H:%M:%S"),
                                    'end_time': end_time,
                                    'end_time_str': end_time.strftime("%Y-%m-%d %H:%M:%S"),
                                    'duration': duration,
                                    'duration_str': str(duration)[:9],
                                    'output': output})

    def _build_image(self, image: ImageInfo):
        """
            docker build --tag=ejana-centos7-prereq .
            docker tag ejana-centos7-prereq eicdev/ejana-centos7-prereq:latest
            docker push eicdev/ejana-centos7-prereq:latest
        """
        # Check if we built dependency
        if self.check_deps:
            if image.depends_on and image.depends_on not in self.built_images_by_name.keys():
                message = f"The image {image.name} depends on {image.depends_on} which has not been built. " \
                          f"Run this script with check-deps=false in order to force {image.name} building"
                print(message)

                # Save it to the logs
                self._append_log('check', image.name, 1, datetime.now(), datetime.now(), [message])                
                return

        # no-cache flag given?
        no_cache_str = "--no-cache" if self.no_cache else ""

        # RUN DOCKER BUILD COMMAND
        os.chdir(image.path)
        retval, start_time, end_time, output = _run(f"docker build {no_cache_str} {image.flags} --tag={image.tag} .")

        # Log the results:
        self._append_log('build', image.name, retval, start_time, end_time, output)

        if retval:
            print(f"(! ! !)   ERROR   (! ! !) build op return code is: {retval}")
        else:
            self.built_images_by_name[image.name] = image
            if self.push_after_build:
                self.push(image)

    def build(self, image_name: str):
        self._build_image(self._images_by_name[image_name])

    def build_all(self):
        for image in self.images:
            self._build_image(image)

    def push(self, name_or_image):
        if isinstance(name_or_image, ImageInfo):
            image = name_or_image
        else:
            image = self._images_by_name[name_or_image]
        os.chdir(image.path)
        retval, start_time, end_time, output = _run(f"docker push {image.tag}")

        # Log the results:
        self._append_log('push', image.name, retval, start_time, end_time, output)       

        if retval:
            print(f"(! ! !)   ERROR   (! ! !) PUSH operation return code is: {retval}")

    def push_built(self):
        for name in self.built_images_by_name.keys():
            self.push(name)

    def push_all(self):
        for name in self._images_by_name.keys():
            self.push(name)


def main():

    def set_images_params(images, root_path, organisation, version):
        for image in images:
            image.tag = f'{organisation}/{image.name}:{version}'
            image.path = f"{root_path}/{image.name}"

    def print_images():
        for image_set_name, image_set in images.items():
            print(image_set_name)
            for image in image_set:
                print(f'   {image}')

    # Argument parsing
    cwd = os.getcwd()
    parser = argparse.ArgumentParser()
    parser.add_argument("--no-cache", help="Use docker --no-cache flag during build", action="store_true")
    parser.add_argument("--tag", help="Set version tag name. latest is set by default", default='latest')
    parser.add_argument("--push", action="store_true", help="If true - push images if built successfully")
    parser.add_argument("--check-deps", type=bool, help="Check that dependency is built", default=True)
    parser.add_argument("command", type=str, help="'list' = list known images. 'devops', 'eic', 'eic-ubuntu' 'all' = build set of images. exact image name = build that image")
    args = parser.parse_args()

    set_images_params(images['devops'], devops_root_path, 'eicdev', args.tag)
    set_images_params(images['eic'], eic_root_path, 'electronioncollider', args.tag)

    if args.command == 'list':
        print_images()
        exit(0)

    if args.command in ['devops', 'eic', 'all']:
        single_image_build = False
        automation = DockerAutomation(images[args.command])
    else:
        single_image_build = True
        automation = DockerAutomation(images['all'])
        automation.check_deps = False

    automation.no_cache = args.no_cache
    automation.push_after_build = args.push

    if single_image_build:
        automation.build(args.command)
    else:
        automation.build_all()

    logs = automation.operation_logs

    os.chdir(cwd)

    print('SUMMARY:')
    print("{:<8} {:<22} {:<9} {:<11} {:<21} {:<21}"
          .format('ACTION', 'IMAGE NAME', 'RETCODE', 'DURATION', 'START TIME', 'END TIME'))
    for log in logs:
        print("{op:<8} {name:<22} {ret_code:<9} {duration_str:<11} {start_time_str:<21} {end_time_str:<21}".format(**log))

    #import json
    #with open('result.json', 'w') as outfile:
    #    json.dump(logs, outfile, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    main()
